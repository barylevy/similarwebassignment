//
//  UITableView-EmptyMessage.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 09/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

import UIKit

extension UITableView {

    public func setEmptyMessage(_ message: String?) {
        guard let text = message else {
            backgroundView = nil
            return
        }
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.bounds.size.width, height: bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = text
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.sizeToFit()

        backgroundView = messageLabel
        separatorStyle = .none
    }
}
