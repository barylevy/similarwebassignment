//
//  String-SW.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 09/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

extension String {
    static let fetching_search_data = "Fetching Search Data ..."
    static let no_results_found = "No Results Found"
    static let global_error = "system error"
    static let loading = "Loading..."
}
