//
//  UIImage-SW.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 09/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import UIKit

extension UIImage {
    static let placholder = UIImage(named: "place_holder")
}
