//
//  UIColor-SW.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import UIKit

extension UIColor {
    static let refreshControl = UIColor.darkGray
    static let photoBorder = UIColor.darkGray

}
