//
//  AppParams.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

class AppParams {

    static let KeyServerName = "server_name"

    static var serverName: ServerName {
        get {
            guard let data = UserDefaults.standard.value(forKey: KeyServerName) as? String,
                let env = ServerName(rawValue: data) else { return ServerName.unsplash}
            return env
        }
        set {
            UserDefaults.standard.set( newValue.rawValue, forKey: KeyServerName)
        }
    }

}
