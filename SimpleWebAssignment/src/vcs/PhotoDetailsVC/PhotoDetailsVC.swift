//
//  PhotoDetailsVC.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import UIKit

class PhotoDetailsVC: UIViewController {

    enum Cells: Int {
        case bigPhoto
        case userDetails

        static var count: Int { return 2 }
    }
    @IBOutlet weak var tableView: UITableView?

    var photoData: PhotoSearchData? {
        didSet {
            tableView?.reloadData()
        }
    }

    override func viewDidLoad() {

        self.tableView?.dataSource = self

        self.tableView?.reloadData()

        self.tableView?.tableFooterView = UIView()
    }
}

extension PhotoDetailsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellType = Cells(rawValue: indexPath.row)
        let provider = ClassFactory.shared.photoStorageProvider
        switch cellType {

        case .bigPhoto:
            let cellName = String(describing: PhotoCell.self)
            let c = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath)
            guard let cell: PhotoCell = c as? PhotoCell else { return c }

            provider?.downloadPhoto(imageView: cell.mainPhotoCell, url: photoData?.urls?.small, progress: nil, completion: { [weak self](image, error) in
                if error != nil {
                    cell.mainPhotoCell.image = .placholder
                    self?.showToast(message: error?.localizedDescription ?? String.global_error)
                } else {
                    self?.tableView?.reloadRows(at: [indexPath], with: .fade)
                }

            })
            cell.photoDescription.text = photoData?.altDescription
            return cell
        case .userDetails:
            let cellName = String(describing: UserCell.self)
            let c = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath)
            guard let cell: UserCell = c as? UserCell else { return c }

            cell.name.text = self.photoData?.user?.name
            cell.userName.text = self.photoData?.user?.username
            cell.bio.text = self.photoData?.user?.bio
            provider?.downloadPhoto(imageView: cell.photoUser,
                                        url: self.photoData?.user?.profileImage?.medium,
                                        progress: nil,
                                        completion: { [weak self](image, error) in
                                            if error != nil {
                                                cell.photoUser.image = .placholder
                                                self?.showToast(message: error?.localizedDescription ?? String.global_error)
                                            } else {
                                                self?.tableView?.reloadRows(at: [indexPath], with: .fade)
                                            }
                                        })

            return cell

        case .none: break

        }
        return UITableViewCell()
    }
}

class PhotoCell: UITableViewCell {

    @IBOutlet weak var mainPhotoCell: UIImageView!
    @IBOutlet weak var photoDescription: UILabel!
}

class UserCell: UITableViewCell {

    @IBOutlet weak var photoUser: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bio: UILabel!
}
