//
//  SearchResultsVC-DataSource.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import UIKit

extension SearchResultsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellName = String(describing: PhotoDataCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath)
        guard let photoDataCell: PhotoDataCell = cell as? PhotoDataCell else { return cell }

        let data: PhotoSearchData = model[indexPath.row]

        photoDataCell.setData(data)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.tableView.deselectRow(at: indexPath, animated: true)

    }
}
// delegate
extension SearchResultsVC {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let index = self.tableView.indexPathForSelectedRow?.row,
            let dest = segue.destination as? PhotoDetailsVC {
            let selectedData: PhotoSearchData = model[index]
            dest.photoData = selectedData
        }
    }

}
