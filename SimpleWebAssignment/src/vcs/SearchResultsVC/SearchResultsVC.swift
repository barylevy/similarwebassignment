//
//  ViewController.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import UIKit

class SearchResultsVC: UIViewController, UISearchBarDelegate, UITableViewDelegate {

    private let refreshControl = UIRefreshControl()

    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var tableView: UITableView!

    var searchProvider: SearchPhotoProviderProtocol?

    var model = [PhotoSearchData]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchProvider = ClassFactory.shared.searchPhotoProvider

        addRefreashControll()

        self.searchBar.delegate = self
        self.tableView.dataSource = self
        self.tableView.delegate = self

        tableView.tableFooterView = UIView()
    }

    func addRefreashControll() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshSearchData(_:)), for: .valueChanged)
        var attributes = [NSAttributedString.Key: AnyObject]()
        attributes[.foregroundColor] = UIColor.refreshControl
        refreshControl.attributedTitle = NSAttributedString(string: String.fetching_search_data, attributes: attributes)
        refreshControl.tintColor = UIColor.refreshControl

    }

    @objc private func refreshSearchData(_ sender: Any) {
        if let textToSearch = searchBar.text {
            doSearchText(textToSearch)
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let textToSearch = searchBar.text {
            doSearchText(textToSearch)
        }
    }

    func doSearchText(_ text: String) {

        // close keyboard
        self.view.endEditing(true)

        let activityIndicator = ActivityIndicator.shared
        activityIndicator.animateActivity(title: String.loading, view: self.view, navigationItem: navigationItem)

        // clar empty message
        self.tableView.setEmptyMessage(nil)

        self.searchProvider?.searchText(text, completion: { [weak self] (_ data: [PhotoSearchData]?, error) in
            guard let self = self else { return }
            self.tableView.setEmptyMessage(data?.count == 0 || data == nil ? String.no_results_found : nil)
            self.model = data ?? [PhotoSearchData]()
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
            activityIndicator.stopAnimating(navigationItem: self.navigationItem)

            if error != nil {
                self.showToast(message: error?.localizedDescription ?? String.global_error)
            }
        })
    }
}

class PhotoDataCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!

    @IBOutlet weak var photoDetails: UILabel!

    override func awakeFromNib() {
        self.photo.layer.cornerRadius = self.photo.frame.size.width / 2.0
        self.photo.clipsToBounds = true
        self.photo.layer.borderWidth = 1.0
        self.photo.layer.borderColor = UIColor.photoBorder.cgColor
        self.photo.image = nil
    }
    func setData(_ data: PhotoSearchData) {

        setPhoto(data.urls?.thumb)

        self.photoDetails.text = data.altDescription

    }
    func setPhoto(_ url: String?) {

        self.photo.image = UIImage.placholder
        ClassFactory.shared.photoStorageProvider?.downloadPhoto(imageView: self.photo, url: url, progress: { (_) in

        }, completion: { (_, _) in

        })
    }
}
