//
//  ClassFactory.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

class ClassFactory {

    private static var sharedFactory: ClassFactory = {
        let factory = ClassFactory()

        return factory
    }()

    private var instanceMap = [String: Any]()

    static var shared: ClassFactory {
        get {
            return sharedFactory
        }
    }

    func resolve<T>(_ type: T.Type) -> T? {
        let name = String(describing: type.self)

        if let val: T = instanceMap[name] as? T {
            return val
        }
        return nil
    }

    func register<T>( _ type: T.Type, impl: T) {
        let name = String(describing: type)

        instanceMap[name] = impl
    }
}
