//
//  IOSFactoryInitializer.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import UIKit

class IOSFactoryInitializer {

    class func initFactoryGeneral( _ container: ClassFactory) {

        container.register(NetworkManagerProtocol.self, impl: NetworkMananger())

        container.register(SearchPhotoProviderProtocol.self, impl: UnsplashPhotoSearchProvider())

        container.register(PhotoStorageProtocol.self, impl: SDWebPhotoStorage())
    }

}
extension ClassFactory {

    var network: NetworkManagerProtocol? {
        get {
            return ClassFactory.shared.resolve(NetworkManagerProtocol.self)
        }
    }

    var searchPhotoProvider: SearchPhotoProviderProtocol? {
        get {
            return ClassFactory.shared.resolve(SearchPhotoProviderProtocol.self)
        }
    }

    var photoStorageProvider: PhotoStorageProtocol? {
        get {
            return ClassFactory.shared.resolve(PhotoStorageProtocol.self)
        }
    }
}
