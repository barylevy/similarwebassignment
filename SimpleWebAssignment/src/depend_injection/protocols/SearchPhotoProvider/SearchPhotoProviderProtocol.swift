//
//  SearchPhotoProviderProtocol.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

typealias CompletionSearch = ([PhotoSearchData]?, Error?) -> Void

protocol SearchPhotoProviderProtocol {

    func searchText(_ textToSearch: String, completion: @escaping CompletionSearch)
}
