//
//  QueryProtocol.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

protocol QueryProtocol: Codable {

    var text: String? { get }
}
