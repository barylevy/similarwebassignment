//
//  NetworkManagerProtocol.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

protocol NetworkManagerProtocol {

    func getPhotoData(query: QueryProtocol?, completion: @escaping([PhotoSearchData]?, Error? ) -> Void)
}
