//
//  PhotoStorageProtocol.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import UIKit

typealias PhotoURL = String
typealias PhotoDownloadCompletion = ((UIImage?, Error?) -> Void)
typealias PhotoUploadCompletion = ((PhotoURL?, Error?) -> Void)

protocol PhotoStorageProtocol {

    func downloadPhoto(imageView: UIImageView, url: PhotoURL?, progress: ((Progress) -> Void)?, completion: @escaping PhotoDownloadCompletion)

}
