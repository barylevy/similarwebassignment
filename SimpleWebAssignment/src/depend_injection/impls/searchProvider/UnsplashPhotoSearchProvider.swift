//
//  SearchImageProvider.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation

struct TextQuery: QueryProtocol {

    var textToSearch: String = ""

    init(_ textToSearch: String) {
        self.textToSearch = textToSearch
    }

    var text: String? { return self.textToSearch }
}
class UnsplashPhotoSearchProvider: SearchPhotoProviderProtocol {

    func searchText(_ textToSearch: String, completion: @escaping CompletionSearch) {

        guard let searchProvider: NetworkManagerProtocol = ClassFactory.shared.network else { return }

        let textQuery = TextQuery(textToSearch)

        searchProvider.getPhotoData(query: textQuery) { (_ result: [PhotoSearchData]?, error) in
            completion(result, error)
        }
    }
}
