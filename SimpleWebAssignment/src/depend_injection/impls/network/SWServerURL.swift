//
//  SWServerURL.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//
import Foundation

enum ServerRoute: String {
    case photos    = "/search/photos"
}

enum SWServerURL {

    case unsplash( route: ServerRoute, searchText: String?, clientId: String)

    static func  urlRunning(route: ServerRoute, searchText: String?, clientId: String) -> URL {
        switch AppParams.serverName {
        case .unsplash:
            return SWServerURL.unsplash(route: route, searchText: searchText, clientId: clientId).url
        }
    }

    var url: URL {
        switch self {

        case .unsplash(let route, let searchText, let clientId):
            let pageNum = "1"
            let per_page = "25"
            let escapedString = searchText?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let str = "\(self.hostName)\(route.rawValue)?page=\(pageNum)&per_page=\(per_page)&client_id=\(clientId)&query=\(escapedString ?? "")"
            return URL(string: str)!
        }
    }

    var hostName: String {
        switch self {
        case .unsplash:   return "https://api.unsplash.com"
        }
    }

    var port: String {
        switch self {
        case .unsplash:   return ""
        }
    }

}
