//
//  NetworkMananger.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation
import Alamofire

class NetworkMananger: NetworkManagerProtocol {

    let unsplashClientId = "c99a7e7599297260b46b7c9cf36727badeb1d37b1f24aa9ef5d844e3fbed76fe"

    init() {

    }
    func getPhotoData(query: QueryProtocol?, completion: @escaping ([PhotoSearchData]?, Error?) -> Void) {

        let url = SWServerURL.urlRunning(route: .photos, searchText: query?.text, clientId: unsplashClientId)

        Alamofire.request(url).responsePhotoDataSearchResponse { response in
            guard let photoDataSearchResponse = response.result.value, response.result.isSuccess else {
                completion(nil, response.error)
                return
            }
            completion(photoDataSearchResponse.results, nil)
        }

    }

}
