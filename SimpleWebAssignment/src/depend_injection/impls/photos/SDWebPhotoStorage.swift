//
//  SDWebPhotoStorage.swift
//  SimilarWebAssignment
//
//  Created by Bary Levy on 08/12/2019.
//  Copyright © 2019 Bary Levy. All rights reserved.
//

import Foundation
import SDWebImage

class SDWebPhotoStorage: PhotoStorageProtocol {

    func downloadPhoto(imageView: UIImageView, url: PhotoURL?, progress: ((Progress) -> Void)?, completion: @escaping PhotoDownloadCompletion) {
        guard let urlNotNil = url,
           let url: URL = URL(string: urlNotNil) else {
           return
        }

        imageView.sd_setImage(with: url) { (image, error, _, _) in

            UIView.transition(with: imageView,
            duration: 0.75,
            options: .transitionCrossDissolve,
            animations: { imageView.image = image },
            completion: { _ in

            })
            completion(image, error)
        }
    }
}
